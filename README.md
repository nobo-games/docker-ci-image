# Docker CI Image

A Docker image with dependencies preinstalled for building nobo.games projects for continuous integration.

# Build

```
docker build -t registry.gitlab.com/nobo-games/docker-ci-image .
```

# Publish

```
docker login registry.gitlab.com <gitlab-username> -p <gitlab-personal-access-token>
docker push registry.gitlab.com/nobo-games/docker-ci-image
```