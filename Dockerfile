FROM rust:1.56-buster

RUN apt-get update && \
    apt-get install -y \
    curl \
    build-essential \
    clang \
    llvm \
    python3-pip && \
    curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh && \
    apt-get remove -y curl && \
    rm -rf /var/lib/apt/lists/
RUN cargo install cargo-make
RUN rustup target add wasm32-unknown-unknown
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN rm requirements.txt
RUN cargo install -f wasm-bindgen-cli --version 0.2.69
RUN rm -rf /var/lib/apt/lists/
